from datetime import datetime
import os

import math
import torch
import torchvision.transforms as transforms
from PIL import Image
from torch.utils.data import Dataset
from torchvision.transforms import functional

from src.utils.utils import OCRLabelConverter


class FixedHeightResize:
    def __init__(self, size):
        self.size = size

    def __call__(self, img):
        w, h = img.size
        aspect_ratio = float(h) / float(w)
        new_w = math.ceil(self.size / aspect_ratio)
        return functional.resize(img, (self.size, new_w))

class Crop:
    def __init__(self, top, left, height, width):
        self.top = top
        self.left = left
        self.height = height
        self.width = width

    def __call__(self, img):
        return functional.crop(img, self.top, self.left, self.height, self.width)

class JochreDataset(Dataset):
    def __init__(self, opt):
        super(JochreDataset, self).__init__()
        self.path = os.path.join(opt.path, opt.imgdir)

        label_path = opt.label_path
        with open(label_path) as fin:
            lines = list( line.split('\t') for line in fin )

        if opt.max_lines > 0:
            lines = lines[0:opt.max_lines]

        print(f'Extracting {len(lines)} images')
        start_time = datetime.now()
        self.images = []
        self.labels = []
        i = 0

        current_image_name = None
        current_image = None
        for line in lines:
            i = i + 1
            image_name = line[0]
            if not current_image_name or current_image_name != image_name:
                current_image_name = image_name
                image_path = os.path.join(self.path, image_name)
                current_image = Image.open(image_path)
                my_transforms = transforms.Compose([
                    transforms.Grayscale(1),
                    transforms.ToTensor(),
                    transforms.Normalize((0.5,), (0.5,)),
                ])

                current_image = my_transforms(current_image)
                delta = datetime.now() - start_time
                print(f'{delta} After initial transforms: image {image_name}, size {current_image.shape}')

            img = current_image.clone()

            box = list(line[2].split(','))
            box = list(map(int, box))

            img = transforms.functional.crop(img, box[1], box[0], box[3], box[2])
            img = transforms.Resize((28, 28)).forward(img)

            img = torch.flatten(img)
            label = line[1]
            self.images.append(img)
            self.labels.append(label)
            if (i % 1000 == 0):
                delta = datetime.now() - start_time
                print(f'{delta} Extracted {i} images')

        delta = datetime.now() - start_time
        print(f'{delta} Extracted {i} images')

        self.nSamples = len(self.labels)
        self.converter = OCRLabelConverter(opt.alphabet)

    def __len__(self):
        return self.nSamples

    def __getitem__(self, index):
        assert index <= len(self), 'index range error'
        img = self.images[index]
        label = self.labels[index]

        label = self.converter.encode_char(label)

        return img, label

class JochreCollator(object):
    
    def __call__(self, batch):

        width = [item['img'].shape[2] for item in batch]
        indexes = [item['idx'] for item in batch]
        imgs = torch.ones([len(batch), batch[0]['img'].shape[0], batch[0]['img'].shape[1], max(width)], dtype=torch.float32)
        for idx, item in enumerate(batch):
            try:
                imgs[idx, :, :, 0:item['img'].shape[2]] = item['img']
            except:
                print(imgs.shape)
        item = {'img': imgs, 'idx':indexes}
        if 'label' in batch[0].keys():
            labels = [item['label'] for item in batch]
            item['label'] = labels
        return item

