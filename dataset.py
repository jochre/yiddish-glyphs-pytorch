import os
import random

import torch
import torch.nn as nn
from torch.utils.data import random_split

import pickle

from src.data.jochre_dataset import JochreDataset
from src.utils.utils import combinedchars, OCRLabelConverter

#@title Define Hyperparameters

input_size = 28*28 # img_size = (28,28) ---> 28*28=784 in total
hidden_size = 500 # number of nodes at hidden layer
num_epochs = 20 # number of times which the entire dataset is passed throughout the model
batch_size = 20 # the size of input data took for one iteration
lr = 1e-3 # size of step
random_seed = 42
log_step = 2
save_path = 'data/models'
data_path = '/home/assaf/deepLearning/datasets/yiddish-labeled-glyphs'
image_path = 'images/train'
label_path = '/home/assaf/deepLearning/datasets/yiddish-labeled-glyphs/labels/train/glyph-to-text.txt'
max_lines = 0

random.seed(random_seed)

class Object(object):
    pass

args = Object()
args.path = data_path
args.imgdir = image_path
args.label_path = label_path
args.max_lines = max_lines

args.alphabet = """!()*,./0123456789:;<>?CL[]_־אאַאָבבּבֿגדהווּזחטייִךככּכֿלםמןנסעףפפּפֿץצקרששׂתתּװױײײַ—’“„•⁄"""
args.nClasses = len(combinedchars(args.alphabet))

args.has_cuda = torch.cuda.is_available()

data = JochreDataset(args)

with open('data/yiddish_glyph_dataset_full.pkl', 'wb') as outp:
    pickle.dump(data, outp, pickle.HIGHEST_PROTOCOL)
