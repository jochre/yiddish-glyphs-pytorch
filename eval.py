import torch
import torch.nn as nn
from torch.utils.data import random_split

from src.data.jochre_dataset import JochreDataset
from src.utils.utils import OCRLabelConverter

# arguments
data_path = 'data/glyphs-sample'
image_path = 'images/val'
label_path = 'data/glyphs-sample/labels/val/glyph-to-text.txt'
model_path = '/home/assaf/deepLearning/yiddish-glyphs/yiddish-glyphs-full-best.ckpt'
batch_size = 20

class Object(object):
    pass

args = Object()
args.path = data_path
args.imgdir = image_path
args.label_path = label_path
args.max_lines = 0

args.alphabet = """!()*,./0123456789:;<>?CL[]_־אאַאָבבּבֿגדהווּזחטייִךככּכֿלםמןנסעףפפּפֿץצקרששׂתתּװױײײַ—’“„•⁄"""

converter = OCRLabelConverter(args.alphabet)
args.nClasses = converter.num_classes


data = JochreDataset(args)

eval_gen = torch.utils.data.DataLoader(dataset = data,
                                        batch_size = batch_size,
                                        shuffle = True)

class Net(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(Net,self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, num_classes)

    def forward(self,x):
        out = self.fc1(x)
        out = self.relu(out)
        out = self.fc2(out)
        return out


loss_function = nn.CrossEntropyLoss()

if torch.cuda.is_available():
    net = torch.load(model_path)
else:
    net = torch.load(model_path, map_location=torch.device('cpu'))
net.eval()

correct = 0
total = 0
for images, labels in eval_gen:
    images = images.view(-1,28*28)
    if torch.cuda.is_available():
        images = images.cuda()
        labels = labels.cuda()

    outputs = net(images)
    validation_loss = loss_function(outputs, labels)
    _, predicted = torch.max(outputs,1)
    correct += (predicted == labels).sum()
    total += labels.size(0)

current_loss = validation_loss.data.item()
accuracy = (100*correct)/(total+1)
print(f'Evaluation loss: {current_loss:.4f}')
print(f'Evaluation Accuracy: {accuracy:.3f}%')