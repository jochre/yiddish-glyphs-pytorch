import os
import random

import torch
import torch.nn as nn
from torch.utils.data import random_split

import pickle

from src.data.jochre_dataset import JochreDataset
from src.utils.utils import combinedchars, OCRLabelConverter

#@title Define Hyperparameters

input_size = 28*28 # img_size = (28,28) ---> 28*28=784 in total
hidden_size = 500 # number of nodes at hidden layer
num_epochs = 20 # number of times which the entire dataset is passed throughout the model
batch_size = 20 # the size of input data took for one iteration
lr = 1e-3 # size of step
random_seed = 42
log_step = 2
save_path = 'data/models'
data_path = 'data/glyphs-sample'
image_path = 'images/train'
label_path = 'data/glyphs-sample/labels/train/glyph-to-text.txt'
dataset_path = 'data/yiddish_glyph_dataset.pkl'


random.seed(random_seed)

class Object(object):
    pass

args = Object()
args.path = data_path
args.imgdir = image_path
args.label_path = label_path

args.alphabet = """!()*,./0123456789:;<>?CL[]_־אאַאָבבּבֿגדהווּזחטייִךככּכֿלםמןנסעףפפּפֿץצקרששׂתתּװױײײַ—’“„•⁄"""
converter = OCRLabelConverter(args.alphabet)
args.nClasses = converter.num_classes

args.has_cuda = torch.cuda.is_available()

if dataset_path:
    with open(dataset_path, 'rb') as inp:
        data = pickle.load(inp)
else:
    data = JochreDataset(args)

#args.collate_fn = JochreCollator()
train_split = int(0.8*len(data))
val_split = len(data) - train_split
args.data_train, args.data_val = random_split(data, (train_split, val_split))
print('Traininig Data Size:{}\nVal Data Size:{}'.format(
    len(args.data_train), len(args.data_val)))

#@title Loading the data

train_gen = torch.utils.data.DataLoader(dataset = args.data_train,
                                        batch_size = batch_size,
                                        shuffle = True)

test_gen = torch.utils.data.DataLoader(dataset = args.data_val,
                                       batch_size = batch_size,
                                       shuffle = False)

#@title Define model class

class Net(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(Net,self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, num_classes)

    def forward(self,x):
        out = self.fc1(x)
        out = self.relu(out)
        out = self.fc2(out)
        return out

net = Net(input_size, hidden_size, args.nClasses)
if torch.cuda.is_available():
    net.cuda()

#@title Define loss-function & optimizer

loss_function = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam( net.parameters(), lr=lr)

#@title Training the model

best_loss = None
for epoch in range(num_epochs):
    for i , (images, labels) in enumerate(train_gen):
        images = images.view(-1,28*28)
        if torch.cuda.is_available():
            images = images.cuda()
            labels = labels.cuda()

        optimizer.zero_grad()
        outputs = net(images)

        loss = loss_function(outputs, labels)
        loss.backward()
        optimizer.step()

        if (i+1) % log_step == 0:
            print('Epoch [%d/%d], Step [%d/%d], Loss: %.4f'
                  %(epoch+1, num_epochs, i+1, len(args.data_train)//batch_size, loss.data.item()))


    print(f'End of epoch {epoch+1}, Loss: {loss.data.item():.4f}')

    save_location = os.path.join(save_path, f'epoch{epoch+1}.ckpt')
    print(f'Saving to {save_location}')
    torch.save(net, save_location)

    correct = 0
    total = 0
    for images, labels in test_gen:
        images = images.view(-1,28*28)
        if torch.cuda.is_available():
            images = images.cuda()
            labels = labels.cuda()

        outputs = net(images)
        validation_loss = loss_function(outputs, labels)
        _, predicted = torch.max(outputs,1)
        correct += (predicted == labels).sum()
        total += labels.size(0)

    current_loss = validation_loss.data.item()
    accuracy = (100*correct)/(total+1)
    print(f'Validation loss at epoch {epoch+1}: {current_loss:.4f}')
    print(f'Accuracy at epoch {epoch+1}: {accuracy:.3f}%')

    if not best_loss or current_loss < best_loss:
        best_loss = current_loss
        print(f'New best validation loss {current_loss:.4f}')
        save_location = os.path.join(save_path, 'best.ckpt')
        print(f'Saving to {save_location}')
        torch.save(net, save_location)